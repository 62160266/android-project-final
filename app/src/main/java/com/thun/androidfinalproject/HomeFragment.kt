package com.thun.androidfinalproject

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.thun.androidfinalproject.databinding.FragmentHomeBinding
import com.thun.androidfinalproject.viewModel.SubjectViewModel
import com.thun.androidfinalproject.viewModel.SubjectViewModelFactory


class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private var price: Double = 0.0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return _binding?.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = SubjectAdapter{
            val action = HomeFragmentDirections.actionHomeFragmentToListFragment(it.id)
            this.findNavController().navigate(action)
        }
        binding.recyclerView.adapter = adapter
        viewModel.getAll.observe(this.viewLifecycleOwner){ item->
            item.let {
                adapter.submitList(it)
            }
        }
        binding.recyclerView.layoutManager = LinearLayoutManager(this.context)

        viewModel.getSumTypePlus.observe(this.viewLifecycleOwner){ item->
            if(item == null){
            }
            else{
                price = item
            }
            bind(price.toString())
        }

        viewModel.getSumTypeMinus.observe(this.viewLifecycleOwner){ item->
            if(item == null){
            }
            else{
                price -= item
            }
            bind(price.toString())
        }

        binding.receiptBtn.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToIncomeFragment()
            view.findNavController().navigate(action)
        }

        binding.expensesBtn.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToSpendingFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    private val viewModel: SubjectViewModel by activityViewModels {
        SubjectViewModelFactory(
            (activity?.application as SubjectApplication).database.subjectDao()
        )
    }

    private fun bind(price:String){
        binding.apply {
            money.text = price
        }
    }
}