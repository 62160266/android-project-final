package com.thun.androidfinalproject.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Subject(
    @PrimaryKey(autoGenerate = true) val id:Int,
    @NonNull @ColumnInfo(name="story")val story: String,
    @NonNull @ColumnInfo(name="money")val money: String,
    @NonNull @ColumnInfo(name="type")val type:String)

