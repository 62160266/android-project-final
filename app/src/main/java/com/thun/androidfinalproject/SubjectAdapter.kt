package com.thun.androidfinalproject

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.thun.androidfinalproject.databinding.ListItemBinding
import com.thun.androidfinalproject.model.Subject


class SubjectAdapter(private val onItemClicked: (Subject)->Unit) :ListAdapter<Subject, SubjectAdapter.SubjectViewHolder>(DiffCallback) {

    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<Subject>() {
            override fun areItemsTheSame(oldItem: Subject, newItem: Subject): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(oldItem: Subject, newItem: Subject): Boolean {
                return oldItem.story == newItem.story
            }
        }
    }

    class SubjectViewHolder(private var binding: ListItemBinding): RecyclerView.ViewHolder(binding.root){
        @SuppressLint("SimpleFormat")
        fun bind(subject: Subject){
            binding.story.text = subject.story
            binding.amount.text = subject.type + subject.money
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubjectViewHolder {
        return SubjectViewHolder(
            ListItemBinding.inflate(
                LayoutInflater.from(
                    parent.context
                )
            )
        )
    }

    override fun onBindViewHolder(holder: SubjectViewHolder, position: Int) {
        val current = getItem(position)
        holder.itemView.setOnClickListener {
            onItemClicked(current)
        }
        holder.bind(current)
    }

}
