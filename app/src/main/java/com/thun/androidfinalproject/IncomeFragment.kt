package com.thun.androidfinalproject

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.thun.androidfinalproject.databinding.FragmentIncomeBinding
import com.thun.androidfinalproject.viewModel.SubjectViewModel
import com.thun.androidfinalproject.viewModel.SubjectViewModelFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [IncomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class IncomeFragment : Fragment() {
    // TODO: Rename and change types of parameters

    private var _binding: FragmentIncomeBinding? = null
    private val binding get() = _binding!!
    private val viewModel: SubjectViewModel by activityViewModels {
        SubjectViewModelFactory(
            (activity?.application as SubjectApplication).database.subjectDao()
        )
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentIncomeBinding.inflate(inflater, container, false)
        return _binding?.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.okBtn.setOnClickListener{
            addNewItem()
        }
    }

    private fun addNewItem(){
        if(isEntryValid()){
            viewModel.addNewTypePlus(
                binding.storyEdit.text.toString(),
                binding.moneyEdit.text.toString()
            )
            val action = IncomeFragmentDirections.actionIncomeFragmentToHomeFragment()
            findNavController().navigate(action)
        }

    }

    private fun isEntryValid(): Boolean{
        return viewModel.isEntryValid(
            binding.storyEdit.text.toString(),
            binding.moneyEdit.text.toString()
        )
    }


}