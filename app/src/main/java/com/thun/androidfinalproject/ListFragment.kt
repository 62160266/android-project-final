package com.thun.androidfinalproject

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.thun.androidfinalproject.databinding.FragmentListBinding
import com.thun.androidfinalproject.model.Subject
import com.thun.androidfinalproject.viewModel.SubjectViewModel
import com.thun.androidfinalproject.viewModel.SubjectViewModelFactory


/**
 * A simple [Fragment] subclass.
 * Use the [ListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ListFragment : Fragment() {
    private val viewModel: SubjectViewModel by activityViewModels {
        SubjectViewModelFactory(
            (activity?.application as SubjectApplication).database.subjectDao()
        )
    }

    lateinit var subject: Subject

    private val navigationArgs: ListFragmentArgs by navArgs()

    private var _binding: FragmentListBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentListBinding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = navigationArgs.position
        viewModel.getSubject(id).observe(this.viewLifecycleOwner) { selectSubject ->
            subject = selectSubject
            bind(subject)
        }
        binding.okBtn.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun showConfirmationDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(android.R.string.dialog_alert_title))
            .setMessage(getString(R.string.delete_question))
            .setCancelable(false)
            .setNegativeButton(getString(R.string.no)) { _, _ -> }
            .setPositiveButton(getString(R.string.yes)) { _, _ ->
                deleteItem()
            }
            .show()
    }

    private fun deleteItem() {
        viewModel.deleteItem(subject)
        val action =ListFragmentDirections.actionListFragmentToHomeFragment()
        findNavController().navigate(action)
    }

    private fun editSubject() {
        val action =ListFragmentDirections.actionListFragmentToEditFragment(subject.id,subject.type)
        this.findNavController().navigate(action)
    }

    private fun bind(subject: Subject) {
        binding.apply {
            storyText.text = subject.story
            priceText.text = subject.money
            type.text = subject.type
            deleteBtn.setOnClickListener { showConfirmationDialog() }
            editBtn.setOnClickListener { editSubject() }
        }
    }
}