package com.thun.androidfinalproject

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.thun.androidfinalproject.databinding.FragmentEditBinding
import com.thun.androidfinalproject.model.Subject
import com.thun.androidfinalproject.viewModel.SubjectViewModel
import com.thun.androidfinalproject.viewModel.SubjectViewModelFactory

class EditFragment : Fragment() {
    private var _binding: FragmentEditBinding? = null
    private val binding get() = _binding!!

    lateinit var subject : Subject

    private val navigationArgs: EditFragmentArgs by navArgs()

    private val viewModel: SubjectViewModel by activityViewModels {
        SubjectViewModelFactory(
            (activity?.application as SubjectApplication).database.subjectDao()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentEditBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var id =navigationArgs.position
        viewModel.getSubject(id).observe(this.viewLifecycleOwner) { selectedItem ->
            subject = selectedItem
            bind(selectedItem)
        }
    }



    private fun isEntryValid(): Boolean {
        return viewModel.isEntryValid(
            binding.storyEdit.text.toString(),
            binding.moneyEdit.text.toString(),
        )
    }

    private fun bind(subject: Subject) {
        binding.apply {
            storyEdit.setText(subject.story, TextView.BufferType.SPANNABLE)
            moneyEdit.setText(subject.money, TextView.BufferType.SPANNABLE)
            okBtn.setOnClickListener { updateItem() }
        }
    }

    private fun updateItem() {
        if (isEntryValid()) {
            viewModel.updateSubject(
                this.navigationArgs.position,
                this.binding.storyEdit.text.toString(),
                this.binding.moneyEdit.text.toString(),
                this.navigationArgs.type
            )
            val action = EditFragmentDirections.actionEditFragmentToHomeFragment()
            findNavController().navigate(action)
        }
    }

}