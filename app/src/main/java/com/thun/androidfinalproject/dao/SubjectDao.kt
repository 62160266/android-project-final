package com.thun.androidfinalproject.dao

import androidx.room.*
import com.thun.androidfinalproject.model.Subject

import kotlinx.coroutines.flow.Flow

@Dao
interface SubjectDao {
    @Query("SELECT * FROM SUBJECT")
    fun getAll(): Flow<List<Subject>>

    @Query("SELECT * FROM Subject WHERE id = :id")
    fun getSubject(id: Int): Flow<Subject>

    @Query("SELECT Sum(money) FROM subject WHERE type = '+'")
    fun getSumTypePlus(): Flow<Double>

    @Query("SELECT Sum(money) FROM subject WHERE type = '-'")
    fun getSumTypeMinus(): Flow<Double>

    @Insert
    suspend fun insert(subject: Subject)

    @Update
    suspend fun update(subject: Subject)

    @Delete
    suspend fun delete(subject: Subject)

}