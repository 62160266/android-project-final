package com.thun.androidfinalproject.viewModel

import androidx.lifecycle.*
import com.thun.androidfinalproject.dao.SubjectDao
import com.thun.androidfinalproject.model.Subject
import kotlinx.coroutines.launch

class SubjectViewModel(private val subjectDao: SubjectDao): ViewModel() {
    val getAll: LiveData<List<Subject>> = subjectDao.getAll().asLiveData()
    fun getSubject(id:Int): LiveData<Subject> {
        return subjectDao.getSubject(id).asLiveData()
    }
    val getSumTypePlus: LiveData<Double> = subjectDao.getSumTypePlus().asLiveData()
    val getSumTypeMinus: LiveData<Double> = subjectDao.getSumTypeMinus().asLiveData()

    fun addNewTypePlus(story:String,price:String){
        val newItem = getNewTypePlus(story, price)
        insertItem(newItem)
    }
    fun addNewTypeMinus(story:String,price:String){
        val newItem = getNewTypeMinus(story, price)
        insertItem(newItem)
    }
    private fun insertItem(subject: Subject) {
        viewModelScope.launch {
            subjectDao.insert(subject)
        }
    }
    private fun getNewTypePlus(story:String,money:String): Subject {
        return Subject(
            id = 0,
            story = story,
            money = money,
            type = "+"
        )
    }

    private fun getNewTypeMinus(story:String,money:String): Subject {
        return Subject(
            id = 0,
            story = story,
            money = money,
            type = "-"
        )
    }

    fun isEntryValid(story: String, price: String): Boolean {
        if (story.isBlank() || price.isBlank()) {
            return false
        }
        return true
    }

    fun deleteItem(subject: Subject) {
        viewModelScope.launch {
            subjectDao.delete(subject)
        }
    }

    private fun getUpdatedSubject(
        id: Int,
        story: String,
        money: String,
        type: String
    ): Subject {
        return Subject(
            id = id,
            story = story,
            money = money,
            type = type
        )
    }

    private fun updateItem(subject: Subject) {
        viewModelScope.launch {
            subjectDao.update(subject)
        }
    }

    fun updateSubject(
        id: Int,
        story: String,
        money: String,
        type: String
    ) {
        val updateSubject = getUpdatedSubject(id, story, money, type)
        updateItem(updateSubject)
    }
}

class SubjectViewModelFactory(private val subjectDao: SubjectDao): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SubjectViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return SubjectViewModel(subjectDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
