package com.thun.androidfinalproject

import android.app.Application
import com.thun.androidfinalproject.database.AppDatabase

class SubjectApplication : Application() {
    val database: AppDatabase by lazy { AppDatabase.getDatabase(this) }
}
